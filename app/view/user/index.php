<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User</title>

    <link rel="stylesheet" href="http://localhost/edukasi-umum/public/css/bootstrap.min.css">
    <script src="http://localhost/edukasi-umum/public/js/bootstrap.min.js" defer></script>
</head>
<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
       <div class="container"><a class="navbar-brand" href="<?= BASE_URL; ?>">Sinau MVC</a><button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav"><a class="nav-item nav-link active" href="<?= BASE_URL; ?>">Home <span class="sr-only">(current)</span></a><a class="nav-item nav-link" href="<?= BASE_URL; ?>/blog">Blog</a><a class="nav-item nav-link"href="<?= BASE_URL; ?>/user">User</a><a class="nav-item nav-link"href="<?= BASE_URL; ?>/login">Login</a></div>
            </div>
        </div>
    </nav>

    <div class="container text-center mt-4">
        <h1>Halaman User</h1><img src="<?= BASE_URL; ?>/img/mainus.jpg" class="rounded-circle shadow w-25">
        <p>Halo, nama saya <?= $data["nama"]; ?>, saya seorang <?= $data["pekerjaan"];?></p>

        <hr class="mt-5">

        <div>
            <div class="row">
                <form action="<?=BASE_URL ?>/user/save" method="POST" class="input-group">
                    <input name="username" type="text" placeholder="Username..." class="w-100 form-control mt-4 shadow"><br>
                    <input name="email" type="email" placeholder="Email..." class="w-100 form-control mt-4 shadow"><br>
                    <input name="password" type="password" placeholder="Password..." class="w-100 form-control mt-4 shadow"><br>
                    <input type="submit" class="btn btn-primary w-100 shadow-lg mt-4" value="Submit">
                </form>
            </div>
        </div>
    </div>
    
</body>
</html>