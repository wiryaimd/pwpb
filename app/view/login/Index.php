<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login App</title>

    
    <link rel="stylesheet" href="http://localhost/edukasi-umum/public/css/bootstrap.min.css">
    <script src="http://localhost/edukasi-umum/public/js/bootstrap.min.js" defer></script>
</head>
<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
       <div class="container"><a class="navbar-brand" href="<?= BASE_URL; ?>">Sinau MVC</a><button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav"><a class="nav-item nav-link active" href="<?= BASE_URL; ?>">Home <span class="sr-only">(current)</span></a><a class="nav-item nav-link" href="<?= BASE_URL; ?>/blog">Blog</a><a class="nav-item nav-link"href="<?= BASE_URL; ?>/user">User</a><a class="nav-item nav-link"href="<?= BASE_URL; ?>/login">Login</a></div>
            </div>
        </div>
    </nav>

    <div class="container mt-5">
        <form action="<?= BASE_URL ?>/login/sign" class="input-group" method="POST" class="row d-flex justify-content-center">
            <input type="text" name="username" placeholder="Username..." class="form-control w-100 col-12"><br>
            <input type="password" name="password" placeholder="Password..." class="form-control w-100 col-12 mt-3"><br>
            <input type="submit" class="btn btn-primary w-25 mt-3" value="Login">
        </form>
    </div>
</body>
</html>