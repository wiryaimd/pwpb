<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard Wiryaimd App</title>

    <link rel="stylesheet" href="http://localhost/edukasi-umum/public/css/bootstrap.min.css">
    <script src="http://localhost/edukasi-umum/public/js/bootstrap.min.js" defer></script>

</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
       <div class="container"><a class="navbar-brand" href="<?= BASE_URL; ?>">Sinau MVC</a><button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div class="navbar-nav"><a class="nav-item nav-link active" href="<?= BASE_URL; ?>">Home <span class="sr-only">(current)</span></a><a class="nav-item nav-link" href="<?= BASE_URL; ?>/blog">Blog</a><a class="nav-item nav-link"href="<?= BASE_URL; ?>/user">User</a><a class="nav-item nav-link"href="<?= BASE_URL; ?>/login">Login</a></div>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="jumbotron mt-4">
            <h1 class="display-4">Hello World Dashboard</h1>
            <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
            <hr class="my-4">
            <p>It uses utility classes for typography and spacing to space content out within the larger container.</p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
        </div>
    </div>
</body>
</html>