<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="http://localhost/edukasi-umum/public/css/bootstrap.min.css">
    <script src="http://localhost/edukasi-umum/public/js/bootstrap.min.js" defer></script>

    <title>Blog</title>
</head>
<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
       <div class="container"><a class="navbar-brand" href="<?= BASE_URL; ?>">Sinau MVC</a><button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
               <div class="navbar-nav"><a class="nav-item nav-link active" href="<?= BASE_URL; ?>">Home <span class="sr-only">(current)</span></a><a class="nav-item nav-link" href="<?= BASE_URL; ?>/blog">Blog</a><a class="nav-item nav-link"href="<?= BASE_URL; ?>/user">User</a><a class="nav-item nav-link"href="<?= BASE_URL; ?>/login">Login</a></div>
            </div>
        </div>
    </nav>
    
    <div class="container border shadow m-5">
        <div class="row">
            <h3>Blog</h3>
            <ul class="list-group p-3" style="list-style: none;">
                <?php foreach($data["blog"] as $blog): ?>
                    <li class="">
                        User Account: <?= $blog["username"] ?>
                    </li>

                    <li class="">
                        Judul: <?= $blog["judul"] ?>
                    </li>

                    <li class="mb-3">
                        Body: <?= $blog["blog"] ?>
                    </li>

                    <hr>
                <?php endforeach ?>
            </ul>

            <form method="POST" action="blog/insert/" class="form-group">
                <input class="form-input my-2" name="input-judul" placeholder="Masukan Judul..."><br>
                <input class="form-input my-2" name="input-body" placeholder="Masukan Body Text..."><br>
                <input class="btn btn-primary mt-1 mb-3" name="submit-form" type="submit" value="Insert Data">
            </form>
        </div>
    </div>
    
</body>
</html>